export default {
  'logout': 'Successfully logged out',
  'auth/user-not-found': 'There is no user with this email!',
  'auth/wrong-password': 'Email and password do not match',
  'auth/email-already-in-use': 'The email address is already in use'
}
